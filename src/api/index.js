import axios from "axios";
import * as url from './apiUrls'

const request = (requestConfig) => {

    return axios.request(requestConfig)
}

export const currentWeatherByCity = (city) => {

    return request({
        method: 'GET',
        url: url.currentWeather(city)
    })
}


