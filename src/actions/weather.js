import {GET_CURRENT_WEATHER, CLEAR_CURRENT_WEATHER} from '../constants/actionTypes'

export const getCurrentWeather = (city) => ({
    type:GET_CURRENT_WEATHER,
    payload: {city}
})
export const clearCurrentWeather = () => ({
    type:CLEAR_CURRENT_WEATHER,
})