import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import {useState} from "react";
import {useDispatch} from "react-redux";
import {clearCurrentWeather, getCurrentWeather} from "../../actions/weather";
import Result from "../../components/weather/Result";
import CityInput from "../../components/weather/CityInput/index";
import styled from './index.module.css'
const Index = () =>  {

    const dispatch = useDispatch();

    const [citiesWeather, setCityWeather] = useState({})

    const handleChange = (event) => {

        setCityWeather((prevState) => ({
            ...prevState,
            [event.target.name]: event.target.value
        }))
    }

    const submitForm = () => {
        dispatch(clearCurrentWeather())
        for (const city in citiesWeather) {
            dispatch(getCurrentWeather({name: city,guessingWeather:citiesWeather[city]}))
        }
    }

    return (
       <div  >
           <div >
               <Card sx={{ minWidth: 275, maxWidth: 600 }} className={styled.mainDiv}>
                   <CardContent>
                       <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
                           Import temp.
                       </Typography>
                       <CityInput name="yerevan" label="Yerevan" changeValue={handleChange}/>
                       <CityInput name="moscow" label="Moscow" changeValue={handleChange}/>
                       <CityInput name="berlin" label="Berlin" changeValue={handleChange}/>
                       <CityInput name="paris" label="Paris" changeValue={handleChange}/>
                       <CityInput name="tbilisi" label="Tbilisi" changeValue={handleChange}/>
                   </CardContent>
                   <CardActions>
                       <Button type="button" variant="contained" color="success" onClick={submitForm}>
                           Submit
                       </Button>
                   </CardActions>
               </Card>
           </div>
           <div>
               <Result/>
           </div>
       </div>
    );
}

export default Index