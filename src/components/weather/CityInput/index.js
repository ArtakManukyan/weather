import {TextField} from "@mui/material";
import styled from './index.module.css'

const CityInput = ({changeValue, name, label}) => {
    return (
        <div>
            <TextField className={styled.cityInput} label={label} name={name} variant="outlined" onChange={changeValue} />
        </div>
    )
}

export default CityInput