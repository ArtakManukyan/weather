import styled from './index.module.css'
import {useSelector} from "react-redux";

const Result = () => {
    const selectWeatherResults = state => state.weatherResults;
    const weatherResults = useSelector(selectWeatherResults)

    const resultsItems = weatherResults.map((weather) => {

        let checkClass = parseInt(weather.currentTemp) + 5 > parseInt(weather.guessingWeather) && parseInt(weather.currentTemp) - 5 < parseInt(weather.guessingWeather)

        return (
            <div className={checkClass ? styled.resultItemRight : styled.resultItemWrong} key={weather.name}>
                <div>
                    {weather.name}
                </div>
                <div>
                    <span>{weather.currentTemp}</span>
                </div>
            </div>
        )
    });
    return (
        <div className={styled.result}>
            {resultsItems}
        </div>
    )
}

export default Result