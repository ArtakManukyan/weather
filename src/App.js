import {
    BrowserRouter as Router,
    Switch,
    Route
} from "react-router-dom";
import Weather from "./pages/weather/index";


function App() {
    return (
        <div className="App">
            <Router>
                <div>
                    <Switch>
                        <Route exact path="/">
                            <Weather/>
                        </Route>
                    </Switch>
                </div>
            </Router>
        </div>
    );
}

export default App;
