import * as api from '../api'
import {call, put, takeEvery} from 'redux-saga/effects'
import {GET_CURRENT_WEATHER, WEATHER_FETCH_SUCCESS} from '../constants/actionTypes'

function* getWeatherByCityName({payload}) {

    try {
        const weather = yield call(api.currentWeatherByCity,payload.city.name);

        const fahrenheitToCelsius = fahrenheit => (fahrenheit - 273.15).toFixed(2);

        yield put({type: WEATHER_FETCH_SUCCESS, city: {...payload.city,currentTemp:fahrenheitToCelsius(weather.data.main.temp)}});
    } catch (e) {

        console.log(e)
        // yield put({type: USERS_FETCH_FAILED, message: e.message});
    }
}


function* watchWeather() {
    yield takeEvery(GET_CURRENT_WEATHER, getWeatherByCityName);
}

const weatherSagas = [watchWeather()]

export default weatherSagas;