import { all } from "redux-saga/effects";
import weather from "./weather";

function* rootSaga() {
    yield all([...weather]);
}

export default rootSaga;
