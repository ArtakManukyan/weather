import {
    CLEAR_CURRENT_WEATHER,
    GET_CURRENT_WEATHER, WEATHER_FETCH_SUCCESS
} from "../constants/actionTypes";

const initState = {
    weatherResults: []
};

const reducer = (state = initState, action) => {
    switch (action.type) {
        case GET_CURRENT_WEATHER:
            return {
                ...state
            };
        case WEATHER_FETCH_SUCCESS:

            return {
                ...state,
                weatherResults: [...state.weatherResults, action.city]
            };
        case CLEAR_CURRENT_WEATHER:

            return {
                ...state,
                weatherResults: []
            };
        default:
            return {...state};
    }
};

export default reducer;